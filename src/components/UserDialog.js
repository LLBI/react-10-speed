import {
  Button,
  Dialog,
  DialogTitle,
  List,
  ListItem,
  ListItemText,
  Typography
} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';

const useStyles = makeStyles({
  list: {
    padding: '0 24px 16px'
  }
});

function DialogComponent(props) {
  const { open, data, onClose, dialogHeading, type } = props;

  const classes = useStyles();

  let content;

  if (type === 'phone') {
    content = data.map(phoneData => (
      <ListItem button>
        <ListItemText
          primary={phoneData.number}
          secondary={phoneData.type ? phoneData.type : null}
        />
      </ListItem>
    ));
  } else if (type === 'location') {
    content = data.map(locationData => (
      <ListItem button>
        <ListItemText
          primary={`${locationData.street}, ${locationData.city}`}
          secondary={`${locationData.state}, ${locationData.country}`}
        />
      </ListItem>
    ));
  } else {
    return;
  }
  return (
    <Dialog
      className={classes.dialog}
      open={open}
      aria-labelledby="dialog-title"
      onClose={onClose}
    >
      <DialogTitle id="dialog-title">
        <Typography variant="h4">{dialogHeading}</Typography>
      </DialogTitle>
      <List className={classes.list}>{content}</List>
    </Dialog>
  );
}

export default function UserDialog(props) {
  const { type, data, dialogHeading } = props;
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const dataLength = props.data.length > 1;

  let content;

  if (type === 'phone') {
    content = data[0].number;
  } else if (type === 'location') {
    content = `${data[0].street}, ${data[0].city}, ${data[0].state}, ${data[0].country}`;
  } else {
    return;
  }

  return (
    <React.Fragment>
      {dataLength ? (
        <React.Fragment>
          <Button variant="outlined" onClick={handleOpen}>
            {props.buttonText}
          </Button>
          <DialogComponent
            open={open}
            dialogHeading={dialogHeading}
            data={data}
            type={type}
            onClose={handleClose}
          />
        </React.Fragment>
      ) : (
        <React.Fragment>{content}</React.Fragment>
      )}
    </React.Fragment>
  );
}
